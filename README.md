ABOUT
Take Home Test - Go Calculator 
A take home exam to demonstrate the skill for the senior software engineer position within the developer experience and devops capability at Esusu
Original code to clone - https://github.com/toddfernandes-esusu/TestCalculator
Objectives include testing framework implementation, test addition, CI/CD integration, test result reporting, documentation

DESIGN AND APPROACH
The first day was dedicated to learning GO and its best testing practices
A block was soon discovered when unable to create integration tests that could immitate os arguments. To navigate around this block and follow best practices is to keep main func as simple as possible with most of the operational calls in a seperate func. 

The second day was creating the Ci/Cd pipeline using Gitlab and Docker containers, test reporting used gotestsum which was recommended by the gitlab documentation. Two stages were created, build and test to perform the required jobs. 

TEST FRAMEWORK
The project called for a seperate module for the go calculator tests, a folder called 'test' was created to store the unit tests. This breaks best practices supported by go lang documentation which strongly recommends keeping unit tests in the same location as the file being tested. Packages were labeled properly and necessary files were 'included' to ensure success. smoke tests and edge case tests were created which could be found in the tests folder. Integration test unfortunately was required to stay in root directory and was unable to operate with a func call from main otherwise. Tags were implemented in all tests to help execute either unit or integration tests during build in the CI CD pipeline. 

USAGE
Testing go calculator was done through two methods
1) Gitlab CI CD which provided a test report at the end of the run 
2) Manual tests in command line
to run all unit tests type go test ./... -v -tags=unit  
to run all integration tests type: go test *.go -v -tags=integration

STATUS
This project is not complete and has some other tasks that would be needed to consider it ready for production 
Bug - integration test in CI CD does not work with reporting, if will succeed if ran without reporting or manually. 

FUTURE WORK 
Research into GO LANG has shown other options to both optimize the application and to greatly expand the test framework. 
- FLUX - is GO's version of choas testing which is able to randomly plug in content into variables
- BENCHMARK - is GO's built in performance testing
These two could be added as seperate jobs for the ci/cd build pipeline as the build promotes into higher environments or more challenging tasks. 
GO also has a built int code coverage utility which could replace SonarCube or other cover coverage applications. 